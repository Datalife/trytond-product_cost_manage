# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.modules.cost_manage.cost_manage import CategorizedMixin


class Template(CategorizedMixin, metaclass=PoolMeta):
    __name__ = 'product.template'

    cost_concept_category = fields.Many2One('product.category',
        'Cost Product Category',
        states={'required': Eval('costs_category', False)},
        depends=['costs_category'])


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'

    # Avoid raise of UserError from MissingFunction
    cost_category_used = fields.Function(
        fields.Many2One('cost.manage.category', 'Cost Category Used'),
        'get_template')
