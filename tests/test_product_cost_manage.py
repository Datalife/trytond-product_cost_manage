# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.pool import Pool
from trytond.exceptions import UserError
from decimal import Decimal


class ProductCostManageTestCase(ModuleTestCase):
    """Test Product Cost Manage module"""
    module = 'product_cost_manage'

    @with_transaction()
    def test_cost_category_used(self):
        'Create template with cost category'
        pool = Pool()
        Template = pool.get('product.template')
        CostCategory = Pool().get('cost.manage.category')
        ModelData = pool.get('ir.model.data')
        ProductUom = pool.get('product.uom')

        consumption = CostCategory(ModelData.get_id('product_cost_manage',
            'cost_category_product'))
        cost_category = CostCategory(name='Cost Manage 1', parent=consumption)
        cost_category.save()
        unit, = ProductUom.search([('name', '=', 'Unit')])
        template = Template(
                name='Template Product',
                list_price=Decimal(10),
                cost_price=Decimal(3),
                default_uom=unit.id,
                cost_category=cost_category
                )
        template.save()
        self.assertEqual(template.cost_category_used, cost_category)

    @with_transaction()
    def test_cost_category_used_costs_category(self):
        'Create template with costs category and product category'
        Template = Pool().get('product.template')
        CostCategory = Pool().get('cost.manage.category')
        ProductCategory = Pool().get('product.category')
        ProductUom = Pool().get('product.uom')

        unit, = ProductUom.search([('name', '=', 'Unit')])
        root_cost_cat, = CostCategory.search([
            ('name', '=', 'Consumption')])
        cost_category = CostCategory(name='Cost 1', parent=root_cost_cat)
        cost_category.save()
        category1, = ProductCategory.create([{
                    'name': 'Product Category 1',
                    'cost_category': cost_category,
                    'cost': True
                    }])
        template = Template(
                name='Template Product',
                list_price=Decimal(10),
                cost_price=Decimal(3),
                default_uom=unit.id,
                costs_category=True,
                cost_concept_category=category1
                )
        template.save()

        self.assertEqual(template.cost_category_used, cost_category)

    @with_transaction()
    def test_cost_category_used_error(self):
        Template = Pool().get('product.template')
        ProductUom = Pool().get('product.uom')

        unit, = ProductUom.search([('name', '=', 'Unit')])
        template = Template(
                name='Template Product',
                list_price=Decimal(10),
                cost_price=Decimal(3),
                default_uom=unit.id,
                )
        template.save()
        with self.assertRaises(UserError):
            self.assert_(template.cost_category_used)


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            ProductCostManageTestCase))
    return suite
