# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import category
from . import product


def register():
    Pool.register(
        category.Category,
        product.Template,
        product.Product,
        module='product_cost_manage', type_='model')
